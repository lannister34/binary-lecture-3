import {createRoomCard, createRoomDOM} from '../helpers/roomHelper.mjs';
import {buildModal} from '../helpers/modalHelper.mjs';
import {deleteAllChildren} from '../helpers/domHelper.mjs';
import {Game} from '../helpers/gameHelper.mjs';
import {CommentBot} from '../helpers/commentHelper.mjs';

(() => {
  const username = sessionStorage.getItem('username');
  const roomsContainer = document.querySelector('.rooms-container');
  const createRoomButton = document.getElementById('createRoom');
  const commentator = new CommentBot();
  let game;
  let currentRoom = null;

  if (!username) {
    window.location.replace('/login');
    return;
  }

  const successLogin = () => {
    socket.on('UPDATE_ROOM', updateRoom);
    socket.on('UPDATE_ROOMS', updateRooms);
    socket.on('LEAVE_COMPLETE', removeCurrentRoom);
    socket.on('FAILED_CREATE_ROOM', redirect.bind(null, 'game'));
    socket.on('FETCH_GAME_DATA', fetchGameData);
    socket.on('UPDATE_COUNTER', updateCountdown);
    socket.on('START_GAME', startGame);
    socket.on('UPDATE_GAME_TIME', updateGameTime);
    socket.on('UPDATE_GAME_PROGRESS', updateGameProgress);
    socket.on('END_GAME', endGame);
    socket.on('UPDATE_COMMENT', commentator.updateContent.bind(commentator));
    getRooms();
    createRoomButton.addEventListener('click', createRoom);
  };

  const redirect = (redirect, message) => {
    window.location.replace(`/redirect?redirect=${redirect}` +
        (message ? `&message=${message}` : ``));
  };

  const logout = (message) => {
    sessionStorage.removeItem('username');
    redirect('login', message);
  };

  const getRooms = () => {
    socket.emit('UPDATE_ROOMS');
  };

  const updateRooms = (rooms) => {
    rooms = new Map(JSON.parse(rooms));
    deleteAllChildren(roomsContainer);
    rooms.forEach((room, roomName) => {
      buildRoom({
        'roomName': roomName,
        'currentUser': username,
        'id': room.id,
        'membersCount': room.members.length,
        'action': joinRoom,
      });
    });

  };

  const createRoom = () => {
    const modal = buildModal({
      'title': 'Create a Room',
      'type': 'form',
      'message': 'Room name',
      'action': socket.emit.bind(socket, 'CREATE_ROOM'),
    });

    modal.run();
  };

  const buildRoom = (roomData) => {
    const room = createRoomCard(roomData);
    roomsContainer.appendChild(room);
    return room;
  };

  const renderRoom = (roomData) => {
    const members = roomData.room.members;
    const currentUser = members.find((user) => user.name === username);
    currentRoom = createRoomDOM({
      'roomName': roomData.name,
      'id': roomData.room.id,
      'members': members,
      'status': currentUser.status,
      'currentUser': username,
      'closeAction': leaveRoom,
      'playAction': setUserStatus,
    });

    if (game) {
      game.initRootNode(currentRoom);
      game.render();
    }

    commentator.initRoot(currentRoom);
  };

  const joinRoom = (roomName, username) => {
    socket.emit('JOIN_ROOM', roomName, username);
  };

  const updateRoom = (roomData) => {
    removeCurrentRoom();
    renderRoom(roomData);
  };

  const leaveRoom = (roomName) => {
    socket.emit('LEAVE_ROOM', roomName, username);
  };

  const removeCurrentRoom = () => {
    currentRoom && currentRoom.remove();
  };

  const setUserStatus = (roomName, status) => {
    socket.emit('READY_STATUS', roomName, username, status);
  };

  const fetchGameData = async (roomName, textId) => {
    const data = await fetch(`/game/texts/${textId}`).then((response) => {
      return response.json();
    }).catch((e) => {
      redirect('game', 'Server error. Try again.');
    });

    game = new Game({
      'currentUser': username,
      'root': currentRoom,
      'text': atob(data.text),
      'socket': socket,
    });

    socket.emit('USER_GOT_DATA', roomName);
  };

  const startGame = () => {
    game && game.startGame();
  };

  const updateCountdown = (value) => {
    game && game.updateCountdown(value);
  };

  const updateGameTime = (value) => {
    game && game.updateGameTime(value);
  };

  const updateGameProgress = (users) => {
    game && game.keyHandler && game.keyHandler.updateProgressBars(users);
  };

  const endGame = () => {
    game = null;
  };

  const socket = io('http://localhost:3002/main', {query: {username}});

  socket.on('SUCCESS_LOGIN', successLogin);
  socket.on('FAILED_LOGIN',
      logout.bind(null, 'User with this username already exist.'));
})();