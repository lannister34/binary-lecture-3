import {append, createElement} from './domHelper.mjs';

export class Game {
  constructor({currentUser, root, text, socket}) {
    this.currentUser = currentUser;
    this.isStarted = false;
    this.text = text;
    this.socket = socket;

    this.initRootNode(root);
    this.render();
  }

  initRootNode(root) {
    this.root = root;
  }

  render() {
    this.gameArea = this.root.querySelector('.game-area');
    const playButton = this.gameArea.querySelector('button.play-button');
    const backButton = this.root.querySelector('.back-button');

    playButton && playButton.remove();
    backButton && backButton.remove();

    if (this.isStarted) {
      this.buildGameTimer();
      this.buildText();

      if (this.keyHandler) {
        this.keyHandler.initTextNode(this.textNode);
        this.keyHandler.render();
      }
    } else {
      this.buildCountdown();
    }
  }

  buildCountdown() {
    if (this.countdownNode) {
      this.countdownNode.remove();
    } else {
      this.countdownNode = createElement({
        'tagName': 'div',
        'className': 'play-button timeout',
      });
    }

    append(this.countdownNode, this.gameArea);
  }

  buildGameTimer() {
    if (this.gameTimerNode) {
      this.gameTimerNode.remove();
    } else {
      this.gameTimerNode = createElement({
        'tagName': 'div',
        'className': 'game-timer',
      });
    }

    append(this.gameTimerNode, this.gameArea);
  }

  buildText() {
    if (this.textNode) {
      this.textNode.remove();
    } else {
      this.textNode = createElement({
        'tagName': 'div',
        'className': 'game-text',
      });
    }

    append(this.textNode, this.gameArea);
  }

  updateGameTime(value) {
    this.updateValue(this.gameTimerNode, value);
  }

  updateCountdown(value) {
    this.updateValue(this.countdownNode, value);
  }

  updateValue(node, value) {
    node.innerHTML = String(value);
  }

  startGame() {
    this.isStarted = true;

    this.countdownNode.remove();
    this.render();

    this.keyHandler = new KeyHandler({
      'currentUser': this.currentUser,
      'socket': this.socket,
      'text': this.text,
      'textNode': this.textNode,
    });
  }
}

class KeyHandler {
  constructor({currentUser, socket, text, textNode}) {
    this.currentUser = currentUser;
    this.socket = socket;
    this.selectedText = '';
    this.progress = 0;

    this.initTextNode(textNode);
    this.render();

    this.symbols = text.split('');
    this.symbolsNodes = this.symbols.map((char) => {
      const node = createElement({
        'tagName': 'span',
        'textContent': char,
      });

      append(node, this.textContentNode);

      return node;
    });

    this.symbolsNodes[0].classList.add('next-symbol');

    this.keydownHandler = (e) => {
      this.next(e);
    };

    window.addEventListener('keydown', this.keydownHandler);
  }

  initTextNode(textNode) {
    this.textNode = textNode;
  }

  render() {
    this.buildContentNode();
    this.getProgressBars();
    this.updateGameProgress(0);
  }

  buildContentNode() {
    if (this.textContentNode) {
      this.textContentNode.remove();
    } else {
      this.textContentNode = createElement({
        'tagName': 'span',
        'className': 'text',
      });
    }

    append(this.textContentNode, this.textNode, 'firstChild');
  }

  getProgressBars() {
    const userNodes = Array.prototype.slice.call(
        document.querySelectorAll('.user-block'));
    this.progressBars = {};

    userNodes.forEach((userNode) => {
      const name = userNode.getAttribute('data-name');
      this.progressBars[name] = userNode.querySelector('.progress');
    });
  }

  next(e) {
    if (e.key === this.symbols[this.progress]) {

      this.symbolsNodes[this.progress].classList.remove('next-symbol');
      this.symbolsNodes[this.progress].classList.add('selected-symbol');

      this.progress++;

      if (this.progress >= this.symbols.length) {
        window.removeEventListener('keydown', this.keydownHandler);
      } else {
        this.symbolsNodes[this.progress].classList.add('next-symbol');
      }

      this.updateGameProgress(1);
    }
  }

  updateProgressBars(users) {
    users.forEach((user) => {
      this.progressBars[user.name] &&
      (this.progressBars[user.name].style.width = (user.progress * 100) + '%');
    });
  }

  updateGameProgress(progress) {
    this.socket.emit('UPDATE_GAME_PROGRESS', this.currentUser, progress);
  }
}