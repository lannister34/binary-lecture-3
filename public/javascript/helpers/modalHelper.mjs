import {createElement, append} from './domHelper.mjs';

export function buildModal({title, type, message, markup, action}) {
  const modal = createElement({
    'tagName': 'div',
    'className': 'modal fade',
    'attributes': {
      'tabindex': '-1',
      'role': 'dialog',
    },
  });

  const modalBackdrop = createElement({
    'tagName': 'div',
    'className': 'modal-backdrop fade',
  });

  const modalDialog = createElement({
    'tagName': 'div',
    'className': 'modal-dialog',
    'attributes': {
      'role': 'document',
    },
  });

  const modalContent = createElement({
    'tagName': 'div',
    'className': 'modal-content',
  });

  const modalHeader = createElement({
    'tagName': 'div',
    'className': 'modal-header',
  });

  const modalTitle = createElement({
    'tagName': 'h5',
    'className': 'modal-title',
    'textContent': title,
  });

  const closeButton = createElement({
    'tagName': 'button',
    'className': 'close',
    'attributes': {
      'data-dismiss': 'modal',
      'aria-label': 'Close',
    },
    'listeners': {
      'click': () => {
        modal.remove();
        modalBackdrop.remove();
      },
    },
  });

  const closeButtonContent = createElement({
    'tagName': 'span',
    'attributes': {
      'aria-hidden': 'true',
    },
    'textContent': '&times;',
  });

  const modalBody = createElement({
    'tagName': 'div',
    'className': 'modal-body',
  });

  if (markup) {
    append(markup, modalBody);
  }

  const modalBodyInput = createElement({
    'tagName': 'input',
    'className': 'form-control',
    'attributes': {
      'placeholder': message,
      'required': '',
    },
  });

  const modalFooter = createElement({
    'tagName': 'div',
    'className': 'modal-footer',
  });

  const applyButton = createElement({
    'tagName': 'button',
    'className': 'btn btn-primary',
    'textContent': 'OK',
    'listeners': {
      'click': () => {
        typeof action === 'function' &&
        action(type === 'form' && modalBodyInput.value);
        modal.remove();
        modalBackdrop.remove();
      },
    },
  });

  const dismissButton = createElement({
    'tagName': 'button',
    'className': 'btn btn-secondary',
    'textContent': 'Close',
    'listeners': {
      'click': () => {
        modal.remove();
        modalBackdrop.remove();
      },
    },
  });

  append(closeButtonContent, closeButton);
  append(closeButton, modalHeader);
  append(modalTitle, closeButton, 'before');
  append(modalHeader, modalContent);

  if (type === 'form') {
    const modalBodyContent = createElement({
      'tagName': 'p',
    });

    append(modalBodyInput, modalBodyContent);
    append(modalBodyContent, modalBody);
    append(dismissButton, modalFooter);
    append(applyButton, modalFooter);
  } else {
    append(applyButton, modalFooter);
  }

  append(modalBody, modalContent);
  append(modalFooter, modalContent);
  append(modalContent, modalDialog);
  append(modalDialog, modal);
  append(modal);
  append(modalBackdrop);

  return {
    'node': modal,
    'run': () => {
      modal.style.display = 'block';
      modal.classList.add('show');
      modalBackdrop.classList.add('show');
    },
  };
}