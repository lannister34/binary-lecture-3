import loginRoutes from './loginRoutes';
import redirectRoutes from './redirectRoutes';
import gameRoutes from './gameRoutes';

export default app => {
  app.use('/login', loginRoutes);
  app.use('/redirect', redirectRoutes);
  app.use('/game', gameRoutes);
  app.get('*', (req, res) => {
    res.render('notFound');
  });
};
