import express from 'express';
import http from 'http';
import socketIO from 'socket.io';
import socketHandler from './socket';
import routes from './routes';
import {STATIC_PATH, PORT} from './config';
import cons from 'consolidate';
import session from 'express-session';
import flash from 'express-flash';

const app = express();
const httpServer = http.Server(app);
const io = socketIO(httpServer);

// Set default ext .dust
app.engine('dust', cons.dust);
app.set('view engine', 'dust');
app.set('views', __dirname + '/views');

// Session Middleware

app.use(session({
  secret: 'secret',
  resave: false,
  saveUninitialized: false,
}));

// Flash messages Middleware

app.use(flash());

app.use(express.static(STATIC_PATH));
routes(app);

app.get('*', (req, res) => {
  res.redirect('/login');
});

socketHandler(io);

httpServer.listen(PORT, () => {
  console.log(`Listen server on port ${PORT}`);
});
