import auth from './main';

export default io => {
  auth(io.of('/main'));
};
