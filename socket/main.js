import * as config from './config.js';
import {texts} from '../data.js';
import {commentBot} from './commentBot.js';

const users = new Map();

/*
    users = {
        "${username}": socket.id
    };
*/

const rooms = new Map();

/*
    rooms = {
        "${name}": {
            "members": [User],
            "currentTime": Number,
            "countdown": Number,
            "status": Boolean,
            "textId": id,
            "textLength": Number,
            "beforeFinish": Boolean
        }
    };
*/

export default io => {
  io.on('connection', socket => {

    const username = socket.handshake.query.username;

    let gameTimerTimeout;

    const createRoom = (roomName) => {
      if (!isUniqueName(rooms, roomName)) {
        socket.emit('FAILED_CREATE_ROOM', 'Room with this name already exist.');
        return;
      }
      if (/^\s*$/.test(roomName)) {
        socket.emit('FAILED_CREATE_ROOM',
            'Name of the room must not be empty.');
        return;
      }
      rooms.set(roomName, {
        'members': [],
        'currentTime': config.SECONDS_FOR_GAME,
        'countdown': config.SECONDS_TIMER_BEFORE_START_GAME,
        'status': false,
        'textId': null,
        'textLength': null,
        'beforeFinish': false,
      });

      joinRoom(roomName);

      updateRooms();

    };

    const joinRoom = (roomName) => {
      socket.join(roomName);

      const room = rooms.get(roomName);

      room.members.push({
        'id': users.get(username),
        'name': username,
        'progress': 0,
        'status': false,
        'time': config.SECONDS_FOR_GAME,
        'gotData': false,
      });

      updateRoom(false, roomName);
      updateRooms();

      room.members.forEach((user) => {
        if (user.name === username) {
          updateComment(true, roomName, commentBot.greeting());
        } else {
          io.to(user.id).emit('UPDATE_COMMENT', commentBot.join([username]));
        }
      });
    };

    const updateRoom = (isLocal, roomName) => {

      isLocal ?
          socket.emit('UPDATE_ROOM', {
            'name': roomName,
            'room': rooms.get(roomName),
          }) :
          io.to(roomName).emit('UPDATE_ROOM', {
            'name': roomName,
            'room': rooms.get(roomName),
          });
    };

    const leaveRoom = (roomName, userName) => {
      removeUserFromRoom(roomName, userName);
      socket.emit('LEAVE_COMPLETE');
      checkRoomStatus(roomName);
      updateRooms();
    };

    const resetRoom = (roomName) => {
      io.in(roomName).clients((error, clients) => {
        if (error) throw error;
        const sockets = clients.map((client) => io.sockets[client]);
        sockets.forEach((item) => {
          item.removeAllListeners('UPDATE_GAME_PROGRESS');
        });
      });

      let room = {
        'members': [],
        'currentTime': config.SECONDS_FOR_GAME,
        'countdown': config.SECONDS_TIMER_BEFORE_START_GAME,
        'status': false,
        'textId': null,
        'textLength': null,
        'beforeFinish': false,
      };

      if (rooms.has(roomName)) {
        room.members = rooms.get(roomName).members.map((user) => {
          user.progress = 0;
          user.status = false;
          user.gotData = false;
          user.time = config.SECONDS_FOR_GAME;

          return user;
        });
      }

      rooms.set(roomName, room);

      updateRoom(false, roomName);
      updateComment(false, roomName, commentBot.greeting());
    };

    const checkRoomStatus = (roomName) => {
      const room = rooms.get(roomName);
      if (!room) {
        return;
      }
      if (room.members.length > 1 &&
          room.members.every((user) => user.status)) {
        room.status = true;
        const textId = Math.floor(Math.random() * Math.floor(texts.length));
        room.textLength = texts[textId].length;
        io.to(roomName).emit('FETCH_GAME_DATA', roomName, textId);
      } else {
        room.status = false;
      }
    };

    const removeUserFromRoom = (roomName, userName) => {
      socket.leave(roomName);
      const room = rooms.get(roomName);
      const members = room.members.filter((user) => user.name !== userName);
      if (members[0]) {
        room.members = members;
        updateRoom(false, roomName);
        updateComment(false, roomName, commentBot.leave([userName]));
      } else {
        rooms.delete(roomName);
      }
    };

    const updateUserStatus = (roomName, userName, status) => {
      const room = rooms.get(roomName);

      room.members = room.members.map((user) => {
        if (user.name === userName) {
          user.status = status;
        }
        return user;
      });

      checkRoomStatus(roomName);

      updateRoom(false, roomName);

      updateRooms();
    };

    const updateRooms = (isLocal) => {
      const availableRooms = new Map();

      rooms.forEach((room, roomName) => {
        const isAvailable = room.members.length <
            config.MAXIMUM_USERS_FOR_ONE_ROOM && !room.status;

        isAvailable && availableRooms.set(roomName, room);
      });

      const serializedAvailableRooms = JSON.stringify(
          Array.from(availableRooms));

      isLocal ?
          socket.emit('UPDATE_ROOMS', serializedAvailableRooms) :
          io.emit('UPDATE_ROOMS', serializedAvailableRooms);
    };

    const checkGameDataStatus = (roomName) => {
      const room = rooms.get(roomName);
      room.members = room.members.map((user) => {
        if (user.name === username) {
          user.gotData = true;
        }
        return user;
      });
      const isAllUsersReady = room.members.every((user) => user.gotData);
      const usernames = room.members.map((user) => user.name);
      if (isAllUsersReady) {
        updateComment(false, roomName, commentBot.start(usernames));
        updateCountdown(roomName);
      }
    };

    const updateCountdown = (roomName) => {
      const room = rooms.get(roomName);
      if (room.countdown <= 0) {
        io.in(roomName).clients((error, clients) => {
          if (error) throw error;
          const sockets = clients.map((client) => io.sockets[client]);
          sockets.forEach((item) => {
            item.on('UPDATE_GAME_PROGRESS',
                updateGameProgress.bind(null, roomName));
          });
        });

        io.to(roomName).emit('START_GAME');
        updateGameTime(roomName);
      } else {
        io.to(roomName).emit('UPDATE_COUNTER', room.countdown);
        room.countdown--;
        setTimeout(updateCountdown, 1000, roomName);
      }
    };

    const updateGameTime = (roomName) => {
      const room = rooms.get(roomName);

      if (room.currentTime <= 0) {
        const sortedUsers = room.members.sort(
            (a, b) => a.progress - b.progress).map((user) => user.name);
        resetRoom(roomName);
        io.to(roomName).emit('END_GAME');
        updateRoom(false, roomName);
        updateComment(false, roomName, commentBot.end(sortedUsers));
      } else {
        const isCommentBotShouldUpdate = config.SECONDS_FOR_GAME !==
            room.currentTime &&
            ((config.SECONDS_FOR_GAME - room.currentTime) %
                config.INTERVAL_COMMENT_BOT_UPDATE) === 0;

        const isCommentBotCanUpdate = config.SECONDS_FOR_GAME !==
            room.currentTime &&
            ((config.SECONDS_FOR_GAME - room.currentTime) %
                Math.ceil(config.INTERVAL_COMMENT_BOT_UPDATE / 3)) === 0;

        if (isCommentBotShouldUpdate) {
          const sortedUsers = room.members.sort(
              (a, b) => a.progress - b.progress).map((user) => user.name);
          updateComment(false, roomName, commentBot.periodic(sortedUsers));
        } else if (isCommentBotCanUpdate) {
          updateComment(false, roomName, commentBot.random());
        }
        io.to(roomName).emit('UPDATE_GAME_TIME', room.currentTime);
        room.currentTime--;
        gameTimerTimeout = setTimeout(updateGameTime, 1000, roomName);
      }
    };

    const updateGameProgress = (roomName, userName, progress) => {
      const room = rooms.get(roomName);

      room.members = room.members.map((user) => {
        if (user.name === userName) {
          user.progress += Number(progress);

          if (user.progress >= room.textLength) {
            user.time = room.currentTime;

            const sortedUsers = room.members.sort(
                (a, b) => a.progress - b.progress).map((user) => user.name);

            updateComment(false, roomName, commentBot.finish(sortedUsers));
          } else if (!room.beforeFinish && room.textLength - user.progress <=
              config.COUNT_OF_SYMBOLS_TO_FINISH) {
            room.beforeFinish = true;

            const sortedUsers = room.members.sort(
                (a, b) => a.progress - b.progress).map((user) => user.name);

            updateComment(false, roomName,
                commentBot.beforeFinish(sortedUsers));
          }
        }
        return user;
      });

      const gameProgressData = room.members.map((user) => {
        const obj = {};
        obj.name = user.name;
        obj.progress = user.progress / room.textLength;
        return obj;
      });

      io.to(roomName).emit('UPDATE_GAME_PROGRESS', gameProgressData);
      if (room.members.every((user) => user.progress >= room.textLength)) {
        const sortedUsers = room.members.sort(
            (a, b) => a.progress - b.progress).map((user) => user.name);
        clearTimeout(gameTimerTimeout);
        resetRoom(roomName);
        io.to(roomName).emit('END_GAME');
        updateRoom(false, roomName);
        updateComment(false, roomName, commentBot.end(sortedUsers));
      }
    };

    const isUniqueName = (mapObject, name) => {
      let isUnique = true;
      mapObject.forEach((value, fieldName) => {
        if (fieldName.toLowerCase() === name.toLowerCase()) {
          isUnique = false;
        }
      });
      return isUnique;
    };

    const updateComment = (isLocal, roomName, comment) => {
      isLocal ?
          socket.emit('UPDATE_COMMENT', comment) :
          io.to(roomName).emit('UPDATE_COMMENT', comment);
    };

    socket.on('disconnect', reason => {

      if (users.get(username) !== socket.id) {
        return;
      }

      users.delete(username);

      rooms.forEach((room, roomName) => {
        removeUserFromRoom(roomName, username);
      });

    });

    socket.on('UPDATE_ROOMS', updateRooms.bind(null, true));

    socket.on('CREATE_ROOM', createRoom);

    socket.on('JOIN_ROOM', joinRoom);

    socket.on('UPDATE_ROOM', updateRoom.bind(null, true));

    socket.on('LEAVE_ROOM', leaveRoom);

    socket.on('READY_STATUS', updateUserStatus);

    socket.on('USER_GOT_DATA', checkGameDataStatus);

    if (!isUniqueName(users, username)) {
      socket.emit('FAILED_LOGIN');
      socket.disconnect(true);
      return;
    }

    users.set(username, socket.id);
    socket.emit('SUCCESS_LOGIN');

  });
};
